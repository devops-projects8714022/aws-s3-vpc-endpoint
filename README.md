# aws-s3-vpc-endpoint



## Getting started
### Документация по развертыванию VPC с VPC Endpoint для S3 с использованием Terraform

Этот проект использует Terraform для создания виртуальной частной облачной среды (VPC) в AWS, включая подсеть, интернет-шлюз и конечную точку VPC (VPC Endpoint) для доступа к S3.

#### Структура проекта

Проект состоит из следующих файлов:

1. **variables.tf**: Файл переменных, где определены параметры конфигурации.
2. **main.tf**: Основной файл конфигурации, в котором описаны ресурсы AWS.
3. **outputs.tf**: Файл, в котором определены выводимые значения.
4. **terraform.tfvars**: (необязательный) Файл для переопределения значений переменных.

#### 1. Файл variables.tf

Файл `variables.tf` содержит определения переменных, которые используются в конфигурации Terraform.

```hcl
variable "aws_region" {
  description = "The AWS region to deploy to"
  type        = string
  default     = "us-west-2"
}

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "subnet_cidr_block" {
  description = "The CIDR block for the subnet"
  type        = string
  default     = "10.0.1.0/24"
}

variable "availability_zone" {
  description = "The availability zone for the subnet"
  type        = string
  default     = "us-west-2a"
}
```

#### 2. Файл main.tf

Файл `main.tf` содержит описание ресурсов AWS, которые будут созданы.

```hcl
provider "aws" {
  region = var.aws_region
}

resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_subnet" "my_subnet" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.availability_zone
}

resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.my_vpc.id
}

resource "aws_route_table" "my_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_igw.id
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.my_route_table.id
}

resource "aws_vpc_endpoint" "s3_endpoint" {
  vpc_id            = aws_vpc.my_vpc.id
  service_name      = "com.amazonaws.${var.aws_region}.s3"
  vpc_endpoint_type = "Gateway"

  route_table_ids = [aws_route_table.my_route_table.id]
}
```

#### 3. Файл outputs.tf

Файл `outputs.tf` используется для вывода идентификаторов созданных ресурсов.

```hcl
output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.my_vpc.id
}

output "subnet_id" {
  description = "The ID of the subnet"
  value       = aws_subnet.my_subnet.id
}

output "vpc_endpoint_id" {
  description = "The ID of the VPC endpoint"
  value       = aws_vpc_endpoint.s3_endpoint.id
}
```

#### 4. Файл terraform.tfvars (необязательный)

Файл `terraform.tfvars` используется для переопределения значений переменных.

```hcl
aws_region          = "us-west-2"
vpc_cidr_block      = "10.0.0.0/16"
subnet_cidr_block   = "10.0.1.0/24"
availability_zone   = "us-west-2a"
```

### Шаги развертывания

1. **Установка Terraform**: Убедитесь, что Terraform установлен на вашем компьютере. Следуйте инструкциям по установке на официальном сайте [Terraform](https://www.terraform.io/downloads.html).

2. **Инициализация Terraform**: Выполните команду для инициализации рабочего каталога Terraform. Это загрузит необходимые провайдеры и подготовит проект к развертыванию.

   ```sh
   terraform init
   ```

3. **Проверка конфигурации**: Выполните команду для проверки конфигурации Terraform и просмотра плана развертывания.

   ```sh
   terraform plan
   ```

4. **Применение конфигурации**: Выполните команду для применения конфигурации и создания ресурсов в AWS.

   ```sh
   terraform apply
   ```

   Подтвердите выполнение команды, введя `yes`, когда будет предложено.

5. **Вывод результатов**: После завершения применения конфигурации, Terraform выведет идентификаторы созданных ресурсов.

### Заключение

Этот проект позволяет создать VPC, подсеть, интернет-шлюз и VPC Endpoint для S3 в AWS с использованием Terraform. Параметры конфигурации можно легко изменить через файл переменных `variables.tf` или файл переопределения значений `terraform.tfvars`.